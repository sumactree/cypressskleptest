// import "./commands";
// import “./namespace”;

// declare namespace Cypress {
//   interface Chainable {
//     /**
//      * Custom command to select DOM element by data-cy attribute.
//      * @example cy.dataCy('greeting')
//      */
//     login(email: string): Chainable<Element>;
//   }
// }

interface IUserPayload {
  name: string;
  password: string;
  email: string;
  firstname: string;
  lastname: string;
  address1: string;
  state: string;
  city: string;
  country: string;
  zipcode: string;
  mobile_number: string;
}

export {};
declare global {
  namespace Cypress {
    interface Chainable {
      login(email: string, password: string): Chainable<void>;
      signIn(userData: IUserPayload): Chainable<void>;
    }
  }
}
