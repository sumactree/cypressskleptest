export class signup_login {
  navigate() {
    cy.visit("/login");
  }
  signIn(
    name: string,
    email: string,
    days: string,
    month: string,
    year: string
  ) {
    cy.get('[data-qa="signup-name"]').type(name);
    cy.get('[data-qa="signup-email"]').type(email);
    cy.get('[data-qa="signup-button"]').click();
    cy.get('[data-qa="create-account"]').click();
    cy.get("input:invalid").should("have.length", 9);
    cy.get('[id="id_gender1"]').click();
    cy.get('[data-qa="days"]').select(days);
    cy.get('[data-qa="months"]').select(month);
    cy.get('[data-qa="years"]').select(year);
    cy.get('[id="newsletter"]').check();
    cy.get('[id="optin"]').check();
    return this;
  }
  validationCheck(userData) {
    const requiredFormFields = [
      {
        selector: "#password",
        key: "password"
      },
      {
        selector: "#first_name",
        key: "name"
      },
      {
        selector: "#last_name",
        key: "lastName"
      },
      {
        selector: "#address1",
        key: "address"
      },
      {
        selector: "#state",
        key: "state"
      },
      {
        selector: "#city",
        key: "city"
      },
      {
        selector: "#zipcode",
        key: "zipcode"
      },
      {
        selector: "#mobile_number",
        key: "mobileNumber"
      }
    ];
    cy.wrap(requiredFormFields).each(({ selector, key }) => {
      cy.get(selector)
        .then(($input) => {
          const validationMessage = ($input[0] as HTMLInputElement)
            .validationMessage;
          expect(validationMessage).to.equal("Please fill in this field.");
        })
        .type(userData[key]);
    });
    cy.get('[data-qa="create-account"]').click();

    cy.contains("h2", "Account Created!");
    // cy.screenshot();
    cy.get('[data-qa="continue-button"]').click();
    return this;
  }
  logOut() {
    cy.get('a[href*="logout"]').click();
    cy.contains("li", "Signup / Login");
    return this;
  }
  logIn(emailAddress, password, name) {
    cy.get('[data-qa="login-email"]').type(emailAddress);
    cy.get('[data-qa="login-password"]').type(password);
    cy.get('[data-qa="login-button"]').click();
    cy.contains("Logged in as " + name);
  }
  deleteAccount(emailAddress, password) {
    cy.get('a[href*="delete_account"]').click();
    cy.contains("h2", "Account Deleted!");
    cy.visit("/login");
    cy.get('[data-qa="login-email"]').type(emailAddress);
    cy.get('[data-qa="login-password"]').type(password);
    cy.get('[data-qa="login-button"]').click();
    cy.contains("p", "Your email or password is incorrect!");
    return this;
  }
}
