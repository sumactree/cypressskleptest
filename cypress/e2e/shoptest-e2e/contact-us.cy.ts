describe("check contact form", () => {
  const emailAddress = `${Date.now()}@aharotest.com`;
  beforeEach(() => {
    cy.visit("/contact_us");
    cy.fixture("user").as("userData");
    cy.fixture("review").as("reviewData");
  });

  it("fills out contact form with the file and returns to home", function () {
    const { userData, reviewData } = this;
    // Check the titles of contact form:
    cy.contains("Contact Us").should("be.visible");
    cy.contains("Get In Touch").should("be.visible");
    // Type name to contact form:
    cy.get('[data-qa="name"]').type(userData.name);
    // Type e-mail to contact form:
    cy.get('[data-qa="email"]').type(emailAddress);
    // Type message subject to contact form:
    cy.get('[data-qa="subject"]').type(reviewData.subject);
    // Type message to contact form:
    cy.get('[data-qa="message"]').type(reviewData.review);
    cy.fixture("/images/Cat.jpg").as("myFixture");
    // Attach the file to contact form:
    cy.get("input[type=file]").selectFile("@myFixture");
    // Submit contact message:
    cy.get('[data-qa="submit-button"]').click();
    // Check if contact message have been submitted successfully:
    cy.contains(
      "Success! Your details have been submitted successfully."
    ).should("be.visible");
    cy.contains("Home").click();
    cy.contains(
      "Full-Fledged practice website for Automation Engineers"
    ).should("be.visible");
  });
});
