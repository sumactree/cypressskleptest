describe("check products site", () => {
  const emailAddress = `${Date.now()}@aharotest.com`;
  beforeEach(() => {
    cy.visit("/products");
    cy.fixture("user").as("userData");
    cy.fixture("review").as("reviewData");
  });

  it("adds the product to the cart and continues shopping", function () {
    cy.visit("/product_details/3");
    cy.get('[id="quantity"]').clear().type("5");
    cy.contains("Add to cart").click();
    cy.contains("Your product has been added to cart.").should("be.visible");
    cy.get(".view-product")
      .find("img")
      .should("have.attr", "src")
      .should("include", "3");
    cy.contains("Continue Shopping").click();
  });

  it("writes a review of product", function () {
    const { userData, reviewData } = this;
    cy.visit("/product_details/3");
    cy.get('[id="name"]').type(userData.name);
    cy.get('[id="email"]').type(emailAddress);
    cy.get('[id="review"]').type(reviewData.review);
    cy.get('[id="button-review"]').click();
    cy.contains("Thank you for your review.").should("be.visible");
  });

  it("searches blue products", function () {
    cy.get('[id="search_product"]').type("Blue");
    cy.get('[id="submit_search"]').click();
    cy.contains("Searched Products").should("be.visible");
    cy.get(".features_items")
      .find(".single-products")
      .then(($value) => {
        cy.get(".single-products").then(($items) => {
          expect($items).to.have.length($value.length);
          expect($items).to.contain("Blue");
        });
      });
  });
});
