describe("processes after adding product to cart for logged user", () => {
  const emailAddress = `${Date.now()}@aharotest.com`;
  beforeEach(() => {
    cy.visit("/login");
    cy.fixture("user").as("userData");
    cy.fixture("card").as("cardData");
  });

  let password;
  it("buys a product and downloads an invoice", function () {
    const { userData, cardData } = this;
    password = userData.password;

    // Sign in to shop:
    cy.signIn({
      name: userData.name,
      password,
      email: emailAddress,
      firstname: userData.name,
      lastname: userData.lastName,
      address1: userData.address,
      state: userData.state,
      city: userData.city,
      country: userData.country,
      zipcode: userData.zipcode,
      mobile_number: userData.mobileNumber
    });

    // Log in to verify that the user has been logged in correctly:
    cy.login(emailAddress, userData.password);

    // Add the product to the cart and check if it has been added:
    cy.get(".single-products").should("not.have.length", 0);
    cy.get(".productinfo").find("p").first().invoke("text").as("productName");
    cy.get("@productName").then((name) => {
      cy.get('a[data-product-id="1"]').first().click();
      cy.get("a:contains(View Cart)").click();
      cy.get(".cart_description").should("contain", name);
    });

    // Buy for the order using credit card:
    cy.get("a:contains(Proceed To Checkout)").click();
    cy.get("a:contains(Place Order)").click();
    cy.get('[data-qa="name-on-card"]').type(cardData.nameOnCard);
    cy.get('[data-qa="card-number"]').type(cardData.cardNumber);
    cy.get('[data-qa="cvc"]').type(cardData.cvc);
    cy.get('[data-qa="expiry-month"]').type(cardData.expiryMonth);
    cy.get('[data-qa="expiry-year"]').type(cardData.expiryYear);
    cy.get('[data-qa="pay-button"]').click();
    cy.get('[data-qa="order-placed"]').should("contain", "Order Placed!");

    // Wait for the invoice and download it:
    cy.window()
      .document()
      .then(function (doc) {
        doc.addEventListener("click", () => {
          setTimeout(function () {
            doc.location.reload();
          }, 5000);
        });
        cy.get("a:contains(Download Invoice)").click();
      });

    // Check if the invoice has been downloaded and return to the home page:
    cy.readFile("cypress/downloads/invoice.txt").should("exist");
    cy.get('[data-qa="continue-button"]').click();
    cy.contains("h2", "Features Items");
  });

  it("deletes user with API", function () {
    cy.request({
      method: "DELETE",
      url: "/api/deleteAccount",
      form: true, // indicates the body should be form urlencoded and sets Content-Type: application/x-www-form-urlencoded headers
      body: {
        password: password,
        email: emailAddress
      }
    }).then((response) => {
      expect(response.status).to.eq(200);
      expect(response.isOkStatusCode).to.eq(true);
      expect(JSON.parse(response.body).responseCode).to.eq(200);
      expect(JSON.parse(response.body).message).to.eq("Account deleted!");
    });
  });

  it("checks if the user has been deleted with API", function () {
    cy.request({
      method: "POST",
      url: "/api/verifyLogin",
      form: true,
      body: {
        password: password,
        email: emailAddress
      }
    }).then((response) => {
      expect(response.status).to.eq(200);
      expect(response.isOkStatusCode).to.eq(true);
      expect(JSON.parse(response.body).responseCode).to.eq(404);
      expect(JSON.parse(response.body).message).to.eq("User not found!");
    });
  });
});
