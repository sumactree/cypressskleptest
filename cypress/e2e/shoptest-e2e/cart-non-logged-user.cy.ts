describe("processes after adding product to cart for non logged user", () => {
  beforeEach(() => {
    cy.visit("/");
  });

  it("adds product to cart", function () {
    // Choose one product:
    cy.get(".single-products").should("not.have.length", 0);
    cy.get(".productinfo").find("p").first().invoke("text").as("productName");
    cy.get("@productName").then((name) => {
      //Click on chosen product to add to cart:
      cy.get('a[data-product-id="1"]').first().click();
      // Click on button ,,View Cart" to check the cart:
      cy.get("a:contains(View Cart)").click();
      // Check if cart contains the added product:
      cy.get(".cart_description").should("contain", name);
    });
  });

  it("deletes product from the cart and redirect to products", function () {
    // Choose one product and add to the cart:
    cy.get('a[data-product-id="1"]').first().click();
    // Click on button ,,View Cart" to check the cart:
    cy.get("a:contains(View Cart)").click();
    // Check if added product is in the cart:
    cy.get('[id="product-1"]').should("have.length", 1);
    // Click on a product to remove it from the cart:
    cy.get('a[data-product-id="1"]').click();
    // Check if cart is empty:
    cy.get('[id="empty_cart"]').should("contain.text", "Cart is empty!");
    // Click "here" to return to the main page:
    cy.get("a:contains(here)").click();
    // Check if you have been redirected to the home page:
    cy.request({
      method: "GET",
      url: "/products"
    }).then((response) => {
      expect(response.status).to.equal(200);
    });
    cy.get(".title").should("contain", "All Products");
  });

  it("adds product to cart and proceed to checkout", function () {
    // Choose one product and add to the cart:
    cy.get('a[data-product-id="2"]').first().click();
    // Click on button ,,View Cart" to check the cart:
    cy.get("a:contains(View Cart)").click();
    // Click the "Proceed to checkout" button to proceed to checkout:
    cy.get("a:contains(Proceed To Checkout)").click();
    // Check if you have been redirected to the checkout:
    cy.get(".modal-body").should(
      "contain.text",
      "Register / Login account to proceed on checkout."
    );
    // Click "Continue in Cart" to proceed with your cart:
    cy.get("button:contains(Continue On Cart)").click();
    //Check if modal with "Continue in Cart" button disappeared:
    cy.get("button:contains(Continue On Cart)").should("be.not.visible");
    cy.get(".modal-body").should("be.not.visible");
    // Click the "Proceed to checkout" button to proceed to checkout:
    cy.get("a:contains(Proceed To Checkout)").click();
    // Click on link contains ,,Register / Login":
    cy.get("a:contains(Register / Login)").click();
    // Check if you have been redirected to the login page:
    cy.url().should("be.equal", `${Cypress.config("baseUrl")}/login`);
  });
});
