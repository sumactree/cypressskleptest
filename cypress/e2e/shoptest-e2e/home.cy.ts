describe("check categories and brands", () => {
  beforeEach(() => {
    cy.visit("/");
  });

  it("checks category Dress of woman clothes", function () {
    // Click on Women category:
    cy.get('a[href*="#Women"]').click();
    // Check if Dress, Tops and Saree are visible:
    cy.get("a:contains(Dress)").should("be.visible");
    cy.get("a:contains(Tops)").should("be.visible");
    cy.get("a:contains(Saree)").should("be.visible");
    // Click on Dress category:
    cy.get("#Women a:contains(Dress)").click();
    // Check if Women - Dress Products are visible:
    cy.contains("h2", "Women - Dress Products");
    // Check if visible products contain word Dress:
    cy.get(".single-products").each((item) => {
      cy.wrap(item).should("contain.text", "Dress");
    });
  });

  it("checks brand and number of products and view product of brand", function () {
    let brandProducts: number;
    cy.get('a[href*="Madame"]').within(() => {
      cy.get("span.pull-right")
        .invoke("text")
        .then((text) => {
          const numberFromBrandProducts = text.match(/[0-9]+/g);
          brandProducts = parseInt(numberFromBrandProducts[0]);
        });
    });
    cy.get('a[href*="Madame"]').click();
    cy.contains("h2", "Brand - Madame Products");
    cy.get(".active").should("have.text", "Madame");
    cy.get(".features_items")
      .find(".col-sm-4")
      .then(($value) => {
        const resultBrandProducts = $value.length;
        expect(brandProducts).to.eq(resultBrandProducts);
      });
    cy.get("a:contains(View Product)").first().click();
    cy.get(".product-information").should("contain.text", "Brand: Madame");
  });

  it("verifies subscription in home page", function () {
    // Generate the e-mail:
    const emailAddress = `${Date.now()}@aharotest.com`;
    // Check if footer contain Subscription text:
    cy.get("#footer").should("contain.text", "Subscription");
    // Type e-mail to field:
    cy.get("#susbscribe_email").type(emailAddress);
    // Click on subscribe button:
    cy.get("#subscribe").click();
    // Check if You have been successfully subscribed:
    cy.get("#footer").should(
      "contain.text",
      "You have been successfully subscribed!"
    );
  });

  it("verifies scroll up using 'Arrow' button", function () {
    cy.scrollTo(0, 1000);
    cy.get("#scrollUp").click();
    cy.window().its("scrollY").should("equal", 0);
  });
});
