import { signup_login } from "../pages/signup_login.cy";
const signUp_logIn = new signup_login();

describe("registration, log in, log out and delete account", () => {
  const emailAddress = `${Date.now()}@aharotest.com`;
  beforeEach(() => {
    signUp_logIn.navigate();
    cy.fixture("user").as("userData");
  });

  it("sign in with checking validations an log out", function () {
    signUp_logIn.signIn(
      this.userData.name,
      emailAddress,
      "2",
      "January",
      "2000"
    );
    signUp_logIn.validationCheck(this.userData);
    signUp_logIn.logOut();
  });

  it("log in and deleting account", function () {
    const { userData } = this;
    signUp_logIn.logIn(emailAddress, userData.password, userData.name);
    signUp_logIn.deleteAccount(emailAddress, userData.password);
  });
});
