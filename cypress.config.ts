import { defineConfig } from "cypress";
// import "./commands.ts";
export default defineConfig({
  downloadsFolder: "cypress/downloads",
  e2e: {
    baseUrl: "https://www.automationexercise.com",
    experimentalRunAllSpecs: true
  }
});
